"""
Standard Analysis Utilities Set
"""
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("Standard Analysis Utilities").getOrCreate()

# Define the function to describe a Spark table
def describe_spark_table(table_name: str) -> None:
    """
    Retrieves and prints the schema of the specified Spark table.

    Args:
        table_name (str): The name of the Spark table to describe.

    Returns:
        None

    Raises:
        ValueError: If the specified table does not exist.
    """
    # Attempt to retrieve the Spark table with the given name
    table_df = spark.table(table_name)

    # Print the table name and its schema
    print("===BEGIN TABLE DESCRIPTION===")
    print(f"Table Name: {table_name}")
    table_df.printSchema()
    print("===END TABLE DESCRIPTION===")
