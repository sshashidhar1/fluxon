# Databricks notebook source
# MAGIC %md
# MAGIC # Gear Guard Effect Size Determination

# COMMAND ----------

import pyspark.sql.functions as F
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import pyspark.sql.functions as F

# COMMAND ----------

signal_table = "main.adhoc.ssh_peregrine_gg_eeap_telemetry_selective_pivoted"

# COMMAND ----------

def plot_sentinel_mode_status(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "XMM_SentinelMode_Status_Mobile", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('vin') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "XMM_SentinelMode_Status_Mobile"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["vin"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    # Get unique states for color mapping
    unique_statuses = selected_pd["XMM_SentinelMode_Status_Mobile"].unique()
    colors = px.colors.qualitative.Pastel
    
    # Assign a color to each unique status
    color_map = {status: colors[i % len(colors)] for i, status in enumerate(unique_statuses)}
    
    for status in unique_statuses:
        status_data = selected_pd[selected_pd["XMM_SentinelMode_Status_Mobile"] == status]
        fig.add_trace(go.Scatter(x=status_data['timestamp'], y=status_data['XMM_SentinelMode_Status_Mobile'],
                                 mode='markers', name=status, marker=dict(color=color_map[status])))
    
    fig.update_layout(title='Sentinel Mode Status Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='Sentinel Mode Status',
                      legend_title='Sentinel Mode Status',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_soe_pack_user_real(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "E_SOE_pack_user_real", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "E_SOE_pack_user_real"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()

    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['E_SOE_pack_user_real'],
                             mode='markers', name='E_SOE_pack_user_real', marker=dict(color='blue')))

    fig.update_layout(title='Scatterplot for E_SOE_pack_user_real Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='E_SOE_pack_user_real',
                      legend_title='State of Energy (SOE)',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_mini_dcdc_output_voltage(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "mini_dcdc_output_voltage_V", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "mini_dcdc_output_voltage_V"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_output_voltage_V'],
                             mode='markers', name='mini_dcdc_output_voltage_V', marker=dict(color='blue')))
    
    fig.update_layout(title='Scatterplot for mini_dcdc_output_voltage_V',
                      xaxis_title='Timestamp',
                      yaxis_title='Voltage (V)',
                      legend_title='Voltage Type',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_clean_bms_state_changes(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "N_BMS_state", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Exclude the specified states
    excluded_states = ['HV_Activation', 'HV_Deactivation', 'Initializing', 
                       'HV_Active_DC_Charger_Deactivation', 'Undefined']
    selected_df = selected_df.filter(~F.col("N_BMS_state").isin(excluded_states))
    
    # Step 4: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "N_BMS_state"])
    
    # Step 5: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 6: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 7: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    # Get unique states for color mapping
    unique_states = selected_pd["N_BMS_state"].unique()
    colors = px.colors.qualitative.Pastel
    
    # Assign a color to each unique state
    color_map = {state: colors[i % len(colors)] for i, state in enumerate(unique_states)}
    
    for state in unique_states:
        state_data = selected_pd[selected_pd["N_BMS_state"] == state]
        fig.add_trace(go.Scatter(x=state_data['timestamp'], y=state_data['N_BMS_state'],
                                 mode='markers', name=state, marker=dict(color=color_map[state])))
    
    fig.update_layout(title='Clean N_BMS_state Changes Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='BMS State',
                      legend_title='BMS State',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_gear_guard_video_mode(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "XMM_GearGuard_Video_Mode", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "XMM_GearGuard_Video_Mode"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    # Get unique statuses for color mapping
    unique_modes = selected_pd["XMM_GearGuard_Video_Mode"].unique()
    colors = px.colors.qualitative.Pastel
    
    # Assign a color to each unique mode
    color_map = {mode: colors[i % len(colors)] for i, mode in enumerate(unique_modes)}
    
    for mode in unique_modes:
        mode_data = selected_pd[selected_pd["XMM_GearGuard_Video_Mode"] == mode]
        fig.add_trace(go.Scatter(x=mode_data['timestamp'], y=mode_data['XMM_GearGuard_Video_Mode'],
                                 mode='markers', name=mode, marker=dict(color=color_map[mode])))
    
    fig.update_layout(title='Gear Guard Video Mode Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='Gear Guard Video Mode',
                      legend_title='Gear Guard Video Mode',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_mini_dcdc_hsd_state(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "mini_dcdc_hsd_state", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "mini_dcdc_hsd_state"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    # Get unique states for color mapping
    unique_states = selected_pd["mini_dcdc_hsd_state"].unique()
    colors = px.colors.qualitative.Pastel
    
    # Assign a color to each unique state
    color_map = {state: colors[i % len(colors)] for i, state in enumerate(unique_states)}
    
    for state in unique_states:
        state_data = selected_pd[selected_pd["mini_dcdc_hsd_state"] == state]
        fig.add_trace(go.Scatter(x=state_data['timestamp'], y=state_data['mini_dcdc_hsd_state'],
                                 mode='markers', name=state, marker=dict(color=color_map[state])))
    
    fig.update_layout(title='mini_dcdc_hsd_state Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='mini_dcdc_hsd_state',
                      legend_title='mini_dcdc_hsd_state',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_battery_voltage(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "mini_dcdc_low_voltage_12v_battery_voltage_V", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "mini_dcdc_low_voltage_12v_battery_voltage_V"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_low_voltage_12v_battery_voltage_V'],
                             mode='markers', name='mini_dcdc_low_voltage_12v_battery_voltage_V', marker=dict(color='blue')))
    
    fig.update_layout(title='12V Battery Voltage Over Time',
                      xaxis_title='Timestamp',
                      yaxis_title='Battery Voltage (V)',
                      legend_title='Voltage Type',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_mini_dcdc_output_voltage(start_date: str, end_date: str, vin: str):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)
    
    # Step 2: Select relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "mini_dcdc_output_voltage_V", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))
    
    # Step 3: Drop rows with null values in the selected columns
    selected_df = selected_df.dropna(subset=["timestamp", "mini_dcdc_output_voltage_V"])
    
    # Step 4: Convert the Spark DataFrame to a Pandas DataFrame
    selected_df = selected_df.dropna(subset=["VIN"])
    selected_pd = selected_df.toPandas()
    
    # Step 5: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")
    
    # Step 6: Plot the scatter plot using Plotly
    fig = go.Figure()
    
    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_output_voltage_V'],
                             mode='markers', name='mini_dcdc_output_voltage_V', marker=dict(color='blue')))
    
    fig.update_layout(title='Scatterplot for mini_dcdc_output_voltage_V',
                      xaxis_title='Timestamp',
                      yaxis_title='Voltage (V)',
                      legend_title='Voltage Type',
                      xaxis=dict(tickangle=45))
    
    fig.show()

def plot_scatterplot(start_date, end_date, vin):
    # Step 1: Read the data from the Spark table
    df = spark.table(signal_table)

    # Step 2: Select the relevant columns and filter by date range and VIN
    selected_df = df.select("timestamp", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_current_mA", "VIN")\
                    .filter((F.col('timestamp') >= start_date) & (F.col('timestamp') <= end_date) & (F.col('VIN') == vin))

    # Step 3: Drop rows with null values
    selected_df = selected_df.dropna()

    # Step 4: Convert columns to numeric types
    selected_df = selected_df.withColumn("mini_dcdc_hsd_output_current_mA", F.col("mini_dcdc_hsd_output_current_mA").cast("float"))
    selected_df = selected_df.withColumn("mini_dcdc_output_current_mA", F.col("mini_dcdc_output_current_mA").cast("float"))

    # Step 5: Convert the Spark DataFrame to a Pandas DataFrame
    selected_pd = selected_df.toPandas()

    # Step 6: Sort values by timestamp
    selected_pd = selected_pd.sort_values(by="timestamp")

    # Step 7: Plot the scatterplot using Plotly with both values on the same graph
    fig = go.Figure()

    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_hsd_output_current_mA'],
                             mode='markers', name='mini_dcdc_hsd_output_current_mA', marker=dict(color='brown')))

    fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_output_current_mA'],
                             mode='markers', name='mini_dcdc_output_current_mA', marker=dict(color='green')))

    fig.update_layout(title='Scatterplot for mini_dcdc_hsd_output_current_mA and mini_dcdc_output_current_mA',
                      xaxis_title='Timestamp',
                      yaxis_title='Current (mA)',
                      legend_title='Current Type',
                      xaxis=dict(tickangle=45))

    fig.show()

# COMMAND ----------

start_date = "2024-05-21"
end_date = "2024-05-29"
# start_date = "2024-05-24"
# end_date = "2024-05-25"


# COMMAND ----------

vin = "7PDSGBBA6SN045450"
# start_date = "2024-05-24 03:00:00"
# end_date = "2024-05-24 15:00:00"
plot_sentinel_mode_status(start_date, end_date, vin) # software
plot_soe_pack_user_real(start_date, end_date, vin)
plot_mini_dcdc_output_voltage(start_date, end_date, vin)
plot_clean_bms_state_changes(start_date, end_date, vin)
plot_mini_dcdc_output_voltage(start_date, end_date, vin)
plot_battery_voltage(start_date, end_date, vin)
plot_mini_dcdc_hsd_state(start_date, end_date, vin)
plot_scatterplot(start_date, end_date, vin)

# COMMAND ----------

vin = "7PDSGBBA5SN045486" # christina yu
# start_date = "2024-05-24 18:00:00"
# end_date = "2024-05-24 21:00:00"
plot_sentinel_mode_status(start_date, end_date, vin) # software
plot_soe_pack_user_real(start_date, end_date, vin)
plot_mini_dcdc_output_voltage(start_date, end_date, vin)
plot_clean_bms_state_changes(start_date, end_date, vin)
plot_mini_dcdc_output_voltage(start_date, end_date, vin)
plot_battery_voltage(start_date, end_date, vin)
plot_mini_dcdc_hsd_state(start_date, end_date, vin)
plot_scatterplot(start_date, end_date, vin)

# COMMAND ----------


