# Databricks notebook source
from analysis import stlutils # standard template utilities
import matplotlib.ticker as ticker
from pyspark.sql.window import Window
from pyspark.sql import functions as F
import plotly.graph_objs as go
import plotly.graph_objs as go
import plotly.io as pio
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pyspark.sql import DataFrame

# COMMAND ----------

def plot_power_draw_with_idle_status(spark_df, vin, colors=None, title='Power Draw over Time with Idle Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where N_BMS_State is 'HV_Active_Idle'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'purple']  # Default colors: blue for power draw, purple for idle points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "N_BMS_State")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "N_BMS_State").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # Highlight points where N_BMS_State is 'HV_Active_Idle'
    idle_df = pd_df[pd_df['N_BMS_State'] == 'HV_Active_Idle']
    fig.add_trace(go.Scatter(
        x=idle_df['timestamp_pst'],
        y=idle_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='Idle',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_idle_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------


