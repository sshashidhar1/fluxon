# Databricks notebook source
from analysis import stlutils # standard template utilities
import matplotlib.ticker as ticker


# COMMAND ----------

stlutils.describe_spark_table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# COMMAND ----------

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# COMMAND ----------

display(df)

# COMMAND ----------

import plotly.graph_objects as go
import pandas as pd
import pyspark.sql.functions as F

# Step 1: Read the data from the Spark table
df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# Step 2: Select the relevant columns
selected_df = df.select("timestamp", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_current_mA")

# Step 3: Drop rows with null values
selected_df = selected_df.dropna()

# Step 4: Convert columns to numeric types
selected_df = selected_df.withColumn("mini_dcdc_hsd_output_current_mA", F.col("mini_dcdc_hsd_output_current_mA").cast("float"))
selected_df = selected_df.withColumn("mini_dcdc_output_current_mA", F.col("mini_dcdc_output_current_mA").cast("float"))

# Step 5: Convert the Spark DataFrame to a Pandas DataFrame
selected_pd = selected_df.toPandas()

# Step 6: Sort values by timestamp
selected_pd = selected_pd.sort_values(by="timestamp")

# Step 7: Plot the line chart using Plotly with both values on the same graph
fig = go.Figure()

fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_hsd_output_current_mA'],
                         mode='lines', name='mini_dcdc_hsd_output_current_mA', line=dict(color='brown')))

fig.add_trace(go.Scatter(x=selected_pd['timestamp'], y=selected_pd['mini_dcdc_output_current_mA'],
                         mode='lines', name='mini_dcdc_output_current_mA', line=dict(color='green')))

fig.update_layout(title='Line Chart for mini_dcdc_hsd_output_current_mA and mini_dcdc_output_current_mA',
                  xaxis_title='Timestamp',
                  yaxis_title='Current (mA)',
                  legend_title='Current Type',
                  xaxis=dict(tickangle=45))

fig.show()


# COMMAND ----------

import pandas as pd
import pyspark.sql.functions as F
import plotly.graph_objects as go

# Step 1: Read the data from the Spark table
df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# Step 2: Select the relevant columns
selected_df = df.select("timestamp", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_current_mA", "mini_dcdc_power_draw_W")

# Step 3: Drop rows with null values
selected_df = selected_df.dropna()

# Step 4: Convert columns to numeric types
selected_df = selected_df.withColumn("mini_dcdc_hsd_output_current_mA", F.col("mini_dcdc_hsd_output_current_mA").cast("float"))
selected_df = selected_df.withColumn("mini_dcdc_output_current_mA", F.col("mini_dcdc_output_current_mA").cast("float"))
selected_df = selected_df.withColumn("mini_dcdc_power_draw_W", F.col("mini_dcdc_power_draw_W").cast("float"))

# Step 5: Convert the Spark DataFrame to a Pandas DataFrame
selected_pd = selected_df.toPandas()

# Step 6: Sort values by timestamp
selected_pd = selected_pd.sort_values(by="timestamp")

# Step 7: Plot the line chart using Plotly with all three values on the same graph
fig = go.Figure()

# Add mini_dcdc_power_draw_W line
fig.add_trace(go.Scatter(x=selected_pd['timestamp'],
                         y=selected_pd['mini_dcdc_power_draw_W'],
                         mode='lines',
                         name='mini_dcdc_power_draw_W',
                         line=dict(color='blue')))

# Customize layout
fig.update_layout(
    title='Line Chart for mini_dcdc_hsd_output_current_mA, mini_dcdc_output_current_mA, and mini_dcdc_power_draw_W',
    xaxis_title='Timestamp',
    yaxis_title='Current (mA) / Power (W)',
    legend_title='Legend',
    xaxis=dict(tickangle=45),
    template='plotly_white',
    autosize=False,
    width=1400,
    height=700
)

# Show plot
fig.show()


# COMMAND ----------

import pandas as pd
import pyspark.sql.functions as F
import plotly.graph_objects as go

# Step 1: Read the data from the Spark table
df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# Step 2: Select the relevant columns
selected_df = df.select("timestamp", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_current_mA", "mini_dcdc_power_draw_W")

# Step 3: Convert the `timestamp` column from UTC to PST
selected_df = selected_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST"))

# Step 4: Drop rows with null values
selected_df = selected_df.dropna()

# Step 5: Convert columns to numeric types
selected_df = selected_df.withColumn("mini_dcdc_hsd_output_current_mA", F.col("mini_dcdc_hsd_output_current_mA").cast("float"))
selected_df = selected_df.withColumn("mini_dcdc_output_current_mA", F.col("mini_dcdc_output_current_mA").cast("float"))
selected_df = selected_df.withColumn("mini_dcdc_power_draw_W", F.col("mini_dcdc_power_draw_W").cast("float"))

# Step 6: Convert the Spark DataFrame to a Pandas DataFrame
selected_pd = selected_df.select("timestamp_pst", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_current_mA", "mini_dcdc_power_draw_W").toPandas()

# Step 7: Sort values by timestamp
selected_pd = selected_pd.sort_values(by="timestamp_pst")

# Step 8: Plot the line chart using Plotly with all three values on the same graph
fig = go.Figure()

# Add mini_dcdc_power_draw_W line
fig.add_trace(go.Scatter(x=selected_pd['timestamp_pst'],
                         y=selected_pd['mini_dcdc_power_draw_W'],
                         mode='lines',
                         name='mini_dcdc_power_draw_W',
                         line=dict(color='blue')))

# Customize layout
fig.update_layout(
    title='Line Chart for mini_dcdc_hsd_output_current_mA, mini_dcdc_output_current_mA, and mini_dcdc_power_draw_W (PST)',
    xaxis_title='Timestamp (PST)',
    yaxis_title='Current (mA) / Power (W)',
    legend_title='Legend',
    xaxis=dict(tickangle=45),
    template='plotly_white',
    autosize=False,
    width=1400,
    height=700
)

# Show plot
fig.show()


# COMMAND ----------

import pandas as pd
import pyspark.sql.functions as F
import plotly.graph_objects as go
from datetime import timedelta

def plot_power_draw_with_status(spark_df, vin, colors=None, title='Power Draw over Time with Engaged Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where XMM_SentinelMode_Status_Mobile is 'Engaged'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    - figsize (tuple, optional): Figure dimension (width, height) in pixels.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'red']  # Default colors: blue for power draw, red for engaged points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "XMM_SentinelMode_Status_Mobile")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "XMM_SentinelMode_Status_Mobile").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # print the pd_df distribution of XMM_GearGuard_Video_Mode
    print(pd_df['XMM_SentinelMode_Status_Mobile'].value_counts())

    # Highlight points where XMM_SentinelMode_Status_Mobile is 'Engaged'
    engaged_df = pd_df[pd_df['XMM_SentinelMode_Status_Mobile'] == 'Engaged']
    fig.add_trace(go.Scatter(
        x=engaged_df['timestamp_pst'],
        y=engaged_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='Engaged Points',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
        autosize=False,
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------

def plot_power_draw_with_shutdown_status(spark_df, vin, colors=None, title='Power Draw over Time with BMS Shutdown Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where N_BMS_State is 'Shutdown'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'red']  # Default colors: blue for power draw, red for shutdown points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "N_BMS_State")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "N_BMS_State").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # Highlight points where N_BMS_State is 'Shutdown'
    shutdown_df = pd_df[pd_df['N_BMS_State'] == 'Shutdown']
    fig.add_trace(go.Scatter(
        x=shutdown_df['timestamp_pst'],
        y=shutdown_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='Shutdown Points',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_shutdown_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------


