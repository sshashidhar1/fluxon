# Databricks notebook source
# MAGIC %md
# MAGIC # Peregrine miniDCDC patterns

# COMMAND ----------

# MAGIC %md
# MAGIC ## Setup

# COMMAND ----------

# MAGIC %md
# MAGIC ### Imports

# COMMAND ----------

from pyspark.sql.window import Window
from pyspark.sql import functions as F
import plotly.graph_objs as go
import plotly.graph_objs as go
import plotly.io as pio
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pyspark.sql import DataFrame

# COMMAND ----------

# MAGIC %md
# MAGIC ### Static Variables

# COMMAND ----------

# Define Rivian color scheme with earth tones
rivian_colors = [
    '#3b7a57',  # Olive Green
    '#f4d03f',  # Yellow
    '#8b4513',  # Saddle Brown
    '#a0522d',  # Sienna
    '#cd853f',  # Peru
    '#d2b48c',  # Tan
    '#deb887',  # BurlyWood
    '#f5deb3',  # Wheat
    '#556b2f',  # Dark Olive Green
    '#6b8e23',  # Olive Drab
    '#bdb76b',  # Dark Khaki
    '#e0ffff',  # Light Cyan
    '#4682b4',  # Steel Blue
    '#5f9ea0',  # Cadet Blue
    '#6495ed',  # Cornflower Blue
]

# COMMAND ----------

# MAGIC %md
# MAGIC ### Function Definitions

# COMMAND ----------

def table_describer(table_name):
    df = spark.table(table_name)
    # print the table name:
    print("===BEGIN TABLE DESCRIPTION===")
    print("Table Name: ", table_name)
    df.printSchema()
    print("===END TABLE DESCRIPTION===")

# COMMAND ----------

def plot_duration_distribution_pie(df, column_name, colors):
    # Group by the specified column and sum the durations
    duration_df = df.groupBy(column_name).sum("duration_to_next_state").toPandas()

    # Rename the columns for convenience
    duration_df.columns = [column_name, 'total_duration']

    # Remove rows with negative or zero total_duration
    duration_df = duration_df[duration_df['total_duration'] > 0]

    if duration_df.empty:
        print(f"No positive durations to plot for {column_name}.")
        return

    # Define the pie chart
    pie_chart = go.Pie(
        labels=duration_df[column_name],
        values=duration_df['total_duration'],
        textinfo='percent+label',
        hole=0.3,  # This creates the donut chart look
        marker=dict(colors=colors[:len(duration_df)]),
        hoverinfo='label+percent+value'
    )

    # Layout of the plot
    layout = go.Layout(
        title=f'Distribution of {column_name.replace("_", " ").title()} by Duration',
        title_x=0.5,
        legend=dict(
            title=column_name.replace("_", " ").title(),
            x=1.05,
            xanchor='left',
            y=0.5,
            yanchor='middle'
        )
    )

    # Create the figure and plot it
    fig = go.Figure(data=[pie_chart], layout=layout)
    pio.show(fig)

# COMMAND ----------

def prepare_and_plot_time_series_with_status(df):
    # Select and convert the necessary columns
    df = df.select(
        "timestamp", 
        # F.col("mini_dcdc_output_current_mA").cast("double").alias("mini_dcdc_mA"),
        F.col("mini_dcdc_hsd_output_current_mA").cast("double").alias("mini_dcdc_mA"),
        "gg_status"
    )
    
    # Remove rows with null or NaN values in the key columns
    df = df.na.drop(subset=["timestamp", "mini_dcdc_mA", "gg_status"])
    
    # Define a window spec for the rolling average
    window_spec = Window.orderBy("timestamp").rowsBetween(-50, 50)  # 100-row centered window
    
    # Calculate rolling average to smooth the data
    df = df.withColumn("smoothed_mA", F.avg("mini_dcdc_mA").over(window_spec))
    
    # Convert to Pandas for plotting
    pd_df = df.select("timestamp", "smoothed_mA", "gg_status").toPandas()
    
    # Ensure the data is sorted by timestamp
    pd_df.sort_values("timestamp", inplace=True)
    
    # Plot using Plotly
    trace = go.Scatter(
        x=pd_df['timestamp'], 
        y=pd_df['smoothed_mA'], 
        mode='lines',
        name='DCDC Output Current',
        line=dict(color='rgba(163, 132, 98, 0.8)', width=2)  # Rivian earth tone
    )
    
    # Find regions where gg_status is 'engaged'
    engaged_df = pd_df[pd_df['gg_status'] == 'Engaged']
    
    engaged_trace = go.Scatter(
        x=engaged_df['timestamp'], 
        y=engaged_df['smoothed_mA'],
        mode='markers',
        name='Engaged',
        marker=dict(color='rgba(255, 99, 71, 0.8)', size=4),  # A more alarming color
    )
    
    layout = go.Layout(
        title="Time Series of mini DCDC Output Current with gg_status Highlight",
        xaxis=dict(title='Timestamp'),
        yaxis=dict(title='mini DCDC Output (mA)'),
        template="plotly_white"
    )
    
    fig = go.Figure(data=[trace, engaged_trace], layout=layout)
    fig.show()

# COMMAND ----------

def prepare_and_plot_time_series_with_status_mpl_hsd(df):
    # Select and convert the necessary columns
    df = df.select(
        "timestamp", 
        F.col("mini_dcdc_hsd_output_current_mA").cast("double").alias("mini_dcdc_mA"),
        "gg_status"
    )
    
    # Remove rows with null or NaN values in the key columns
    df = df.na.drop(subset=["timestamp", "mini_dcdc_mA", "gg_status"])
    
    # Define a window spec for the rolling average
    window_spec = Window.orderBy("timestamp").rowsBetween(-50, 50)  # 100-row centered window
    
    # Calculate rolling average to smooth the data
    df = df.withColumn("smoothed_mA", F.avg("mini_dcdc_mA").over(window_spec))
    
    # Convert to Pandas for plotting
    pd_df = df.select("timestamp", "smoothed_mA", "gg_status").toPandas()
    
    # Ensure the data is sorted by timestamp
    pd_df.sort_values("timestamp", inplace=True)
    
    # Plot using Matplotlib
    fig, ax = plt.subplots(figsize=(10, 6))
    
    ax.plot(pd_df['timestamp'], pd_df['smoothed_mA'], label='DCDC Output Current', color='#A38462', linewidth=2)
    
    # Highlight regions where gg_status is 'engaged'
    engaged_df = pd_df[pd_df['gg_status'] == 'Engaged']
    ax.scatter(engaged_df['timestamp'], engaged_df['smoothed_mA'], color='#FF6347', s=10, label='Engaged')
    
    # Set titles and labels
    ax.set_title("Time Series of mini DCDC Output Current with gg_status Highlight")
    ax.set_xlabel('Timestamp')
    ax.set_ylabel('mini DCDC Output (mA)')
    
    # Format x-axis for dates
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    
    # Rotate date labels for better readability
    plt.xticks(rotation=45)
    
    # Add a legend
    ax.legend()
    
    # Apply Rivian colorscheme
    fig.patch.set_facecolor('white')
    ax.set_facecolor('white')
    
    plt.tight_layout()
    plt.show()

# COMMAND ----------

def prepare_and_plot_time_series_with_status_mpl_regular(df):
    # Select and convert the necessary columns
    df = df.select(
        "timestamp", 
        F.col("mini_dcdc_output_current_mA").cast("double").alias("mini_dcdc_mA"),
        "gg_status"
    )
    
    # Remove rows with null or NaN values in the key columns
    df = df.na.drop(subset=["timestamp", "mini_dcdc_mA", "gg_status"])
    
    # Define a window spec for the rolling average
    window_spec = Window.orderBy("timestamp").rowsBetween(-50, 50)  # 100-row centered window
    
    # Calculate rolling average to smooth the data
    df = df.withColumn("smoothed_mA", F.avg("mini_dcdc_mA").over(window_spec))
    
    # Convert to Pandas for plotting
    pd_df = df.select("timestamp", "smoothed_mA", "gg_status").toPandas()
    
    # Ensure the data is sorted by timestamp
    pd_df.sort_values("timestamp", inplace=True)
    
    # Plot using Matplotlib
    fig, ax = plt.subplots(figsize=(10, 6))
    
    ax.plot(pd_df['timestamp'], pd_df['smoothed_mA'], label='DCDC Output Current', color='#A38462', linewidth=2)
    
    # Highlight regions where gg_status is 'engaged'
    engaged_df = pd_df[pd_df['gg_status'] == 'Engaged']
    ax.scatter(engaged_df['timestamp'], engaged_df['smoothed_mA'], color='#FF6347', s=10, label='Engaged')
    
    # Set titles and labels
    ax.set_title("Time Series of mini DCDC Output Current with gg_status Highlight")
    ax.set_xlabel('Timestamp')
    ax.set_ylabel('mini DCDC Output (mA)')
    
    # Format x-axis for dates
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    
    # Rotate date labels for better readability
    plt.xticks(rotation=45)
    
    # Add a legend
    ax.legend()
    
    # Apply Rivian colorscheme
    fig.patch.set_facecolor('white')
    ax.set_facecolor('white')
    
    plt.tight_layout()
    plt.show()

# COMMAND ----------

def filter_by_vin(df: DataFrame, vin_value: str) -> DataFrame:
    """
    Filter the DataFrame based on a specific VIN.

    Args:
    df (DataFrame): The input Spark DataFrame.
    vin_value (str): The VIN value to filter by.

    Returns:
    DataFrame: A Spark DataFrame filtered by the specified VIN.
    """
    # Filter the dataframe where the 'vin' column matches the vin_value
    filtered_df = df.filter(df['vin'] == vin_value)
    
    return filtered_df

# COMMAND ----------

# MAGIC %md
# MAGIC ## Analysis

# COMMAND ----------

table_name = "main.adhoc.ss_gg_calculated_timestamps_p_6"
df = spark.table(table_name)

# COMMAND ----------

vin_to_filter = "7FCTGBAA6PMD0191X"  # Example VIN
df = filter_by_vin(df, vin_to_filter)

# COMMAND ----------

plot_duration_distribution_pie(df, "context_user_proximity", rivian_colors)
plot_duration_distribution_pie(df, "vehicle_state", rivian_colors)
plot_duration_distribution_pie(df, "gg_status", rivian_colors)
plot_duration_distribution_pie(df, "N_BMS_State", rivian_colors)

# COMMAND ----------

prepare_and_plot_time_series_with_status_mpl_hsd(df)
prepare_and_plot_time_series_with_status_mpl_regular(df)

# COMMAND ----------

table_name = "main.adhoc.ss_gg_calculated_timestamps_p_6"
df = spark.table(table_name)
vin_to_filter = "7FCTGFAA8PMT0266X"  # Example VIN
df = filter_by_vin(df, vin_to_filter)
plot_duration_distribution_pie(df, "context_user_proximity", rivian_colors)
plot_duration_distribution_pie(df, "vehicle_state", rivian_colors)
plot_duration_distribution_pie(df, "gg_status", rivian_colors)
plot_duration_distribution_pie(df, "N_BMS_State", rivian_colors)
prepare_and_plot_time_series_with_status_mpl_hsd(df)
# prepare_and_plot_time_series_with_status_mpl_regular(df)

# COMMAND ----------

table_name = "main.adhoc.ss_gg_calculated_timestamps_p_6"
df = spark.table(table_name)
vin_to_filter = "7FCTGJAA0RMT0331X"  # Example VIN
df = filter_by_vin(df, vin_to_filter)
plot_duration_distribution_pie(df, "context_user_proximity", rivian_colors)
plot_duration_distribution_pie(df, "vehicle_state", rivian_colors)
plot_duration_distribution_pie(df, "gg_status", rivian_colors)
plot_duration_distribution_pie(df, "N_BMS_State", rivian_colors)
prepare_and_plot_time_series_with_status_mpl_hsd(df)

# COMMAND ----------

 
