# Databricks notebook source
from pyspark.sql import functions as F
from analysis import stlutils # standard template utilities
import matplotlib.ticker as ticker
from pyspark.sql.window import Window
from pyspark.sql import functions as F
import plotly.graph_objs as go
import plotly.graph_objs as go
import plotly.io as pio
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pyspark.sql import DataFrame

# COMMAND ----------

from pyspark.sql import functions as F
from pyspark.sql.window import Window
import pandas as pd
import plotly.express as px

def plot_rp_soc_pack_user_over_time(table_name):
    # Step 1: Read and preprocess data
    df = spark.table(table_name)
    
    # Convert `rp_SOC_pack_user` to double
    df = df.withColumn("rp_SOC_pack_user", F.col("rp_SOC_pack_user").cast("double"))
    
    # Step 2: Group data by 10-minute intervals
    df = df.withColumn("time_bin", F.window(F.col("timestamp"), "10 minutes"))
    
    # Extract start time of each window
    df = df.withColumn("time_start", F.col("time_bin").getItem("start"))
    
    # Select necessary columns
    df = df.select("time_start", "rp_SOC_pack_user")
    
    # Step 3: Collect results into a Pandas DataFrame
    pdf = df.toPandas()
    
    # Step 4: Plotting
    fig = px.scatter(pdf, x="time_start", y="rp_SOC_pack_user", 
                     title="RP SOC Pack User Over Time",
                     labels={"time_start": "Time", "rp_SOC_pack_user": "RP SOC Pack User"},
                     color_discrete_sequence=px.colors.qualitative.Dark24)

    # Update layout to use Rivian color scheme (earth tones)
    fig.update_layout(
        title="RP SOC Pack User Over Time",
        xaxis_title="Time",
        yaxis_title="RP SOC Pack User",
        template="plotly_white"
    )

    fig.show()

# Call the function with the provided table name
plot_rp_soc_pack_user_over_time("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")


# COMMAND ----------

def plot_power_draw_with_charging_status(spark_df, vin, colors=None, title='Power Draw over Time with Charging Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'orange']  # Default colors: blue for power draw, orange for charging points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "N_BMS_State")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "N_BMS_State").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # Highlight points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'
    charging_df = pd_df[pd_df['N_BMS_State'].isin(['HV_Active_AC_Charging', 'HV_Active_DC_Charging'])]
    fig.add_trace(go.Scatter(
        x=charging_df['timestamp_pst'],
        y=charging_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='AC/DC Charging',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_charging_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------

def plot_power_draw_with_charging_status(spark_df, vin, colors=None, title='Power Draw over Time with Charging Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'orange']  # Default colors: blue for power draw, orange for charging points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "N_BMS_State")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "N_BMS_State").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # Highlight points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'
    charging_df = pd_df[pd_df['N_BMS_State'].isin(['HV_Active_AC_Charging'])]
    fig.add_trace(go.Scatter(
        x=charging_df['timestamp_pst'],
        y=charging_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='AC/DC Charging',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_charging_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------

def plot_power_draw_with_charging_status(spark_df, vin, colors=None, title='Power Draw over Time with Charging Status'):
    """
    Plots the power draw (mini_dcdc_power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot.
    - title (str, optional): Title of the graph.
    
    Returns:
    - None: The function creates and displays a Plotly plot.
    """
    
    # Use the specified colors or default colors if none are provided
    if colors is None:
        colors = ['blue', 'orange']  # Default colors: blue for power draw, orange for charging points

    # Further filter for the specific VIN
    filtered_df = spark_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and convert `timestamp` from UTC to PST
    filtered_df = filtered_df.select("timestamp", "mini_dcdc_power_draw_W", "N_BMS_State")
    filtered_df = filtered_df.withColumn("timestamp_pst", F.from_utc_timestamp(F.col("timestamp"), "PST")).drop("timestamp")
    
    # Drop rows with null values
    filtered_df = filtered_df.dropna()
    
    # Convert the Spark DataFrame to a Pandas DataFrame
    pd_df = filtered_df.select("timestamp_pst", "mini_dcdc_power_draw_W", "N_BMS_State").toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp_pst'
    pd_df.sort_values("timestamp_pst", inplace=True)
    
    # Plotting
    fig = go.Figure()

    # Main line plot for power draw
    fig.add_trace(go.Scatter(
        x=pd_df['timestamp_pst'],
        y=pd_df['mini_dcdc_power_draw_W'],
        mode='lines',
        name='Power Draw (W)',
        line=dict(color=colors[0])
    ))

    # Highlight points where N_BMS_State is 'HV_Active_AC_Charging' or 'HV_Active_DC_Charging'
    charging_df = pd_df[pd_df['N_BMS_State'].isin(['HV_Active_DC_Charging'])]
    fig.add_trace(go.Scatter(
        x=charging_df['timestamp_pst'],
        y=charging_df['mini_dcdc_power_draw_W'],
        mode='markers',
        name='AC/DC Charging',
        marker=dict(color=colors[1], size=3)
    ))

    # Customize layout
    fig.update_layout(
        title=title,
        xaxis_title='Timestamp (PST)',
        yaxis_title='Power Draw (W)',
        legend_title='Legend',
        xaxis=dict(tickangle=45),
        template='plotly_white',
    )

    # Show plot
    fig.show()

df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
# Usage example
plot_power_draw_with_charging_status(spark_df=df, vin='7PDSGBBA9SN045393')


# COMMAND ----------

# Step 1: Load the table
df = spark.table("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")

# Step 2: Convert E_SOE_pack_user_real to double
df = df.withColumn("E_SOE_pack_user_real", F.col("E_SOE_pack_user_real").cast("double"))

# Step 3: Select relevant columns
df_selected = df.select("date", "E_SOE_pack_user_real")

# Step 4: Aggregate Data by date
df_aggregated = df_selected.groupBy("date").agg(F.avg("E_SOE_pack_user_real").alias("avg_E_SOE_pack_user_real"))

# Step 5: Convert to Pandas
pdf = df_aggregated.orderBy("date").toPandas()

# Step 6: Plot Data
import plotly.express as px

fig = px.line(pdf, x='date', y='avg_E_SOE_pack_user_real', title='Average E_SOE_pack_user_real Over Time',
              labels={'date': 'Date', 'avg_E_SOE_pack_user_real': 'Average E_SOE_pack_user_real'},
              template='plotly_white')

# Customizing plot with Rivian colors
fig.update_layout(
    title='Average E_SOE_pack_user_real Over Time',
    xaxis_title='Date',
    yaxis_title='Average E_SOE_pack_user_real',
    legend_title='',
    font=dict(
        family='Arial, sans-serif',
        size=12,
        color='DarkSlateGray'
    )
)

fig.show()


# COMMAND ----------


