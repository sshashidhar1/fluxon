# Databricks notebook source
from analysis import stlutils # standard template utilities
from pyspark.sql import functions as F
import pandas as pd
import matplotlib.pyplot as plt
from pyspark.sql import DataFrame
import matplotlib.dates as mdates

# COMMAND ----------

table_name = "main.adhoc.ss_gg_calculated_timestamps_and_new_values_p_9"

# COMMAND ----------

stlutils.describe_spark_table(table_name)

# COMMAND ----------

def plot_vehicle_currents(spark_df: DataFrame, vin: str, colors=None, title='Current Measurements over Time', figsize=(10, 6)):
    """
    Plots the currents (pure_HV_current_mA, mini_dcdc_output_current_mA, mini_dcdc_hsd_output_current_mA)
    for a specific vehicle identified by its VIN from a Spark DataFrame, limiting the data to the last two days.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot. Uses Matplotlib 'tab10' if not provided.
    - title (str, optional): Title of the graph. Default is 'Current Measurements over Time'.
    - figsize (tuple, optional): Figure dimension (width, height) in inches. Default is (10, 6).
    
    Returns:
    - None: The function creates and displays a matplotlib plot.
    """
    
    # Use the 'tab10' matplotlib color scheme if no custom colors are provided
    if colors is None:
        colors = plt.cm.tab10(range(10))
    
    # Determine the maximum date in the dataset
    max_date = spark_df.select(F.max("date")).collect()[0][0]

    # Filter the dataframe to the last two days
    last_two_days_df = spark_df.filter(
        (F.col('date') >= F.date_sub(F.lit(max_date), 1)) & 
        (F.col('date') <= max_date)
    )
    
    # Further filter for the specific VIN
    filtered_df = last_two_days_df.filter(F.col('vin') == vin).toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp'
    filtered_df.sort_values(by='timestamp', inplace=True)
    
    # Convert the relevant columns to numeric, setting non-numeric values to NaN
    filtered_df['mini_dcdc_output_current_mA'] = pd.to_numeric(filtered_df['mini_dcdc_output_current_mA'], errors='coerce')
    filtered_df['mini_dcdc_hsd_output_current_mA'] = pd.to_numeric(filtered_df['mini_dcdc_hsd_output_current_mA'], errors='coerce')
    filtered_df['pure_HV_current_mA'] = pd.to_numeric(filtered_df['pure_HV_current_mA'], errors='coerce')
    
    # Plotting
    plt.figure(figsize=figsize)
    
    plt.plot(filtered_df['timestamp'], filtered_df['pure_HV_current_mA'], label='Pure HV Current (mA)', color=colors[0], linewidth=2)
    plt.plot(filtered_df['timestamp'], filtered_df['mini_dcdc_output_current_mA'], label='Mini DCDC Output Current (mA)', color=colors[1], linewidth=2)
    plt.plot(filtered_df['timestamp'], filtered_df['mini_dcdc_hsd_output_current_mA'], label='Mini DCDC HSD Output Current (mA)', color=colors[2], linewidth=2)
    
    plt.xlabel('Timestamp')
    plt.ylabel('Current (mA)')
    plt.title(title)
    plt.legend()
    
    # Optional: Rotate the dates for better readability
    plt.xticks(rotation=45)
    plt.tight_layout()
    
    # Show the plot
    plt.show()


# COMMAND ----------

df = spark.table("main.adhoc.ss_gg_calculated_timestamps_and_new_values_p_9")
vin = "7FCTGBAA6PMD0191X" # good data
# vin = "7FCTGFAA8PMT0266X" # GG power spike
# vin = "7PDSGGBA7PMT0286X" # GG caused power spike
# vin = "7FCTGGAA9PMT0274X" # GG caused power spike
# vin = "7FCTGJAA0RMT0331X" # - NO GG
# vin = "7FCTGJAA8RMT0330X" # - No GG, idling power draw
# vin = "7PDSGBBA7PMD0201X" # draws without GG
# vin = "7FCTGEAA1PMT0277X" # complete erratic data
# vin = "7FCTGBAA4PMD0126X" # blanked data
# vin = "7FCTGBAA3PMD0122X" # blanked data
# vin = "7PDSGBBA8PMD0145X" # blanked
plot_vehicle_currents(df, vin)

# COMMAND ----------

def plot_power_draw(spark_df: DataFrame, vin: str, colors=None, title='Power Draw over Time', figsize=(10, 6)):
    """
    Plots the power draw (power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot. Uses Matplotlib 'tab10' if not provided.
    - title (str, optional): Title of the graph. Default is 'Power Draw over Time'.
    - figsize (tuple, optional): Figure dimension (width, height) in inches. Default is (10, 6).
    
    Returns:
    - None: The function creates and displays a matplotlib plot.
    """
    
    # Use the 'tab10' matplotlib color scheme if no custom colors are provided
    if colors is None:
        colors = [plt.cm.tab10(0)]  # Use the first color from the tab10 palette
    
    # Determine the maximum date in the dataset
    max_date = spark_df.select(F.max("date")).collect()[0][0]

    # Filter the dataframe to the last two days
    last_two_days_df = spark_df.filter(
        (F.col('date') >= F.date_sub(F.lit(max_date), 1)) & 
        (F.col('date') <= max_date)
    )
    
    # Further filter for the specific VIN
    filtered_df = last_two_days_df.filter(F.col('vin') == vin).toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp'
    filtered_df.sort_values(by='timestamp', inplace=True)
    
    # Convert the 'power_draw_W' column to numeric, setting non-numeric values to NaN
    filtered_df['power_draw_W'] = pd.to_numeric(filtered_df['power_draw_W'], errors='coerce')
    
    # Plotting
    plt.figure(figsize=figsize)
    
    plt.plot(filtered_df['timestamp'], filtered_df['power_draw_W'], label='Power Draw (W)', color=colors[0], linewidth=2)
    
    plt.xlabel('Timestamp')
    plt.ylabel('Power Draw (W)')
    plt.title(title)
    plt.legend()
    
    # Optional: Rotate the dates for better readability
    plt.xticks(rotation=45)
    plt.tight_layout()
    
    # Show the plot
    plt.show()


# COMMAND ----------

plot_power_draw(df, vin)

# COMMAND ----------

def plot_power_draw_with_status(spark_df: DataFrame, vin: str, colors=None, title='Power Draw over Time with Engaged Status', figsize=(10, 6)):
    """
    Plots the power draw (power_draw_W) for a specific vehicle identified by its VIN from a Spark DataFrame,
    limiting the data to the last two days and marking points where gg_status is 'Engaged'.
    
    Parameters:
    - spark_df (DataFrame): Spark DataFrame containing the vehicle data.
    - vin (str): Vehicle Identification Number to filter the data.
    - colors (list, optional): List of colors to use in the plot. Uses Matplotlib 'tab10' if not provided.
    - title (str, optional): Title of the graph. Default is 'Power Draw over Time with Engaged Status'.
    - figsize (tuple, optional): Figure dimension (width, height) in inches. Default is (10, 6).
    
    Returns:
    - None: The function creates and displays a matplotlib plot.
    """
    
    # Use the 'tab10' matplotlib color scheme if no custom colors are provided
    if colors is None:
        colors = [plt.cm.tab10(0), plt.cm.tab10(3)]  # Use the first and fourth colors from the tab10 palette
    
    # Determine the maximum date in the dataset
    max_date = spark_df.select(F.max("date")).collect()[0][0]

    # Filter the dataframe to the last two days
    last_two_days_df = spark_df.filter(
        (F.col('date') >= F.date_sub(F.lit(max_date), 1)) & 
        (F.col('date') <= max_date)
    )
    
    # Further filter for the specific VIN
    filtered_df = last_two_days_df.filter(F.col('vin') == vin)
    
    # Select necessary columns and drop rows with nulls in these columns
    filtered_df = filtered_df.select("timestamp", "power_draw_W", "gg_status").na.drop(subset=["timestamp", "power_draw_W", "gg_status"])
    
    # Convert to Pandas DataFrame for plotting
    pd_df = filtered_df.toPandas()
    
    # Ensure the DataFrame is sorted by 'timestamp'
    pd_df.sort_values("timestamp", inplace=True)
    
    # Convert 'power_draw_W' to numeric, setting non-numeric values to NaN
    pd_df['power_draw_W'] = pd.to_numeric(pd_df['power_draw_W'], errors='coerce')
    
    # Plotting
    fig, ax = plt.subplots(figsize=figsize)
    
    # Main line plot for power draw
    ax.plot(pd_df['timestamp'], pd_df['power_draw_W'], label='Power Draw (W)', color=colors[0], linewidth=2)
    
    # Highlight points where gg_status is 'Engaged'
    engaged_df = pd_df[pd_df['gg_status'] == 'Engaged']
    ax.scatter(engaged_df['timestamp'], engaged_df['power_draw_W'], color=colors[1], s=10, label='Engaged Points', zorder=5)
    
    # Set titles and labels
    ax.set_title(title)
    ax.set_xlabel('Timestamp')
    ax.set_ylabel('Power Draw (W)')
    
    # Format the x-axis for dates
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    
    # Rotate date labels for better readability
    plt.xticks(rotation=45)
    
    # Add a legend
    ax.legend()
    
    # Set face color for consistency
    fig.patch.set_facecolor('white')
    ax.set_facecolor('white')
    
    plt.tight_layout()
    plt.show()

# COMMAND ----------

plot_power_draw_with_status(df, vin)

# COMMAND ----------


