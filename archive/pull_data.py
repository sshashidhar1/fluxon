"""
The purpose of this file is to pull data from the high fidelity and low fidelity tables to analyze the patterns of range loss in the Peregrine VINs
"""
# imports
# Standard library imports
import functools

# Third party imports
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.window import Window
from typing import List, Dict
import logging

# Configure logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)

def create_configured_spark_session() -> SparkSession:
    """
    Initializes and configures a Spark session with specific settings optimized for Delta tables.

    Returns:
        SparkSession: A Spark session configured with specific optimizations for Delta tables.
    """
    # Create or retrieve the existing Spark session
    spark = SparkSession.builder.getOrCreate()
    
    # Set configuration properties specific to Delta tables optimization
    spark.conf.set("spark.databricks.delta.optimizeWrite.enabled", "true")
    spark.conf.set("spark.databricks.delta.autoOptimize.optimizeWrite", "true")
    
    # Return the configured Spark session
    return spark

def pull_signals(
    spark: SparkSession,
    table_low_latency: str,
    table_high_fidelity: str,
    table_extracted_signals: str,
    all_signals: List[str],
    original_conditions: F.Column,
    relevant_cols: List[str],
    new_signals: List[str],
    car_ownership_id_to_vin: Dict[str, str]
) -> None:
    """
    Extracts vehicle signals from source tables and writes combined signals into a destination table.

    Args:
      spark (SparkSession): The Spark session object.
      table_low_latency (str): Name of the source table for low latency data.
      table_high_fidelity (str): Name of the source table for high fidelity data.
      table_extracted_signals (str): Name of the destination table for extracted signals.
      all_signals (List[str]): List of signal names to be extracted from the low latency table.
      original_conditions (F.Column): Filter conditions to apply on the source data.
      relevant_cols (List[str]): List of columns to select from the source table.
      new_signals (List[str]): List of new signal names to be extracted from the high fidelity table.
      car_ownership_id_to_vin (Dict[str, str]): Mapping from car ownership ID to VIN.
    """
    TIMESTAMP_GRANULARITY = 10  # Granularity for timestamp adjustment in seconds

    # Process the low latency signals
    low_latency_signals_df = spark.table(table_low_latency)
    non_null_conditions = [F.col(col).isNotNull() for col in relevant_cols[:-1]]
    combined_conditions = original_conditions & functools.reduce(
        lambda a, b: a & b, non_null_conditions
    )
    
    low_latency_signals_df = (
        low_latency_signals_df
        .filter(F.col("signal_name").isin(all_signals))
        .select(relevant_cols)
        .where(combined_conditions)
        .groupBy("vin", "date", "timestamp")
        .pivot("signal_name", all_signals)
        .agg(F.first("property_value"))
        .withColumn("timestamp", F.col("timestamp").cast("timestamp"))
    )

    # Process the high fidelity signals
    high_fidelity_signals_df = spark.table(table_high_fidelity)
    ownership_case_statement = "CASE " + " ".join(
        [f"WHEN id = '{k}' THEN '{v}'" for k, v in car_ownership_id_to_vin.items()]
    ) + " END"
    
    high_fidelity_signals_df = (
        high_fidelity_signals_df
        .filter(F.col("id").isin(list(car_ownership_id_to_vin.keys())))
        .withColumn("vin", F.expr(ownership_case_statement))
        .filter(F.col("signal.name").isin(new_signals))
        .select(
            "vin",
            F.col("timestamp"),
            F.col("signal.name").alias("signal_name"),
            F.col("signal.value").alias("property_value")
        )
        .where(original_conditions)
        .withColumn(
            "timestamp",
            F.from_unixtime(F.floor(F.unix_timestamp("timestamp") / TIMESTAMP_GRANULARITY) * TIMESTAMP_GRANULARITY, "yyyy-MM-dd HH:mm:ss")
        )
        .withColumn("timestamp", F.col("timestamp").cast("timestamp"))
        .groupBy("vin", "timestamp")
        .pivot("signal_name", new_signals)
        .agg(F.first("property_value"))
        .orderBy("vin", "timestamp")
    )

    # Combine and process the dataframes
    combined_signals_df = (
        low_latency_signals_df
        .unionByName(high_fidelity_signals_df, allowMissingColumns=True)
        .withColumn("date", F.to_date(F.col("timestamp")))
        .orderBy("vin", "timestamp")
    )

    # Write the combined data to the destination table
    combined_signals_df.write.format("delta").mode("overwrite").option("overwriteSchema", "true").saveAsTable(table_extracted_signals)

def process_signals(
    spark, 
    table_extracted_signals: str, 
    table_processed_signals: str, 
    all_signals: list
):
    """
    Processes the signals by performing forward fills and detecting changes using Spark DataFrame operations.

    Args:
      spark (SparkSession): Spark session object to allow for DataFrame operations.
      table_extracted_signals (str): Name of the table containing the extracted signals.
      table_processed_signals (str): Name of the table to store the processed signals.
      all_signals (List[str]): List of signal names to be processed.
    
    Returns:
      None: The function writes the processed data to a table and does not return a value.
    """
    # Load the extracted signals into a DataFrame
    signal_data_frame = spark.table(table_extracted_signals)

    # Define a window spec for forward filling missing values
    forward_fill_window = (
        Window.partitionBy("vin")
        .orderBy("timestamp")
        .rowsBetween(Window.unboundedPreceding, 0)
    )

    # Fill forward the missing values for each signal
    for signal_name in all_signals:
        signal_data_frame = signal_data_frame.withColumn(
            signal_name, 
            F.last(signal_name, ignorenulls=True).over(forward_fill_window)
        )

    # Define a window spec for detecting changes by comparing with the previous value
    change_detection_window = Window.partitionBy("vin").orderBy("timestamp")
    
    for signal_name in all_signals:
        signal_data_frame = signal_data_frame.withColumn(
            f"prev_{signal_name}", 
            F.lag(F.col(signal_name)).over(change_detection_window)
        )

    # Create a condition to filter rows where any signal has changed
    change_detected_condition = None
    for signal_name in all_signals:
        current_condition = F.col(signal_name) != F.col(f"prev_{signal_name}")
        change_detected_condition = (
            current_condition if change_detected_condition is None 
            else change_detected_condition | current_condition
        )

    # Filter out rows where no change is detected across all signals
    signal_data_frame = signal_data_frame.filter(
        change_detected_condition | F.col(f"prev_{all_signals[0]}").isNull()
    )

    # Drop the temporary previous value columns
    for signal_name in all_signals:
        signal_data_frame = signal_data_frame.drop(f"prev_{signal_name}")

    # Write the processed data out to the specified table
    signal_data_frame.write.format("delta").mode("overwrite").saveAsTable(table_processed_signals)

def calculate_timestamps(spark: SparkSession, table_processed_signals: str, table_calculated_timestamps: str) -> None:
    """
    Process signal data to handle and adjust timestamps that span across different days.

    This function loads signal data, identifies signals that cross day boundaries, duplicates and adjusts
    these signals, and finally computes the duration to the next state for each signal. The results are
    saved back to a specified table.

    Args:
        spark (SparkSession): The Spark session object used to execute Spark operations.
        table_processed_signals (str): The name of the table that contains the processed signals.
        table_calculated_timestamps (str): The name of the target table to save the calculated timestamps.

    Returns:
        None
    """
    # Load the initial signal data from the specified table
    signal_data = spark.table(table_processed_signals)

    # Define a window specification to order data by timestamp within each vehicle identifier (vin)
    window_spec = Window.partitionBy("vin").orderBy("timestamp")

    # Ensure the timestamp column is cast to timestamps
    signal_data = signal_data.withColumn("timestamp", F.col("timestamp").cast("timestamp"))

    # Compute the next timestamp for each signal within the partition defined by window_spec
    signal_data = signal_data.withColumn("next_timestamp", F.lead(F.col("timestamp")).over(window_spec))

    # Convert next_timestamp to a date for day comparison
    signal_data = signal_data.withColumn("next_date", F.to_date("next_timestamp"))

    # Identify signals that span across different days
    cross_day_signals = signal_data.filter("date != next_date")

    # Signals within the same day do not need adjustment
    same_day_signals = signal_data.filter("date == next_date")

    # Duplicate the entries for signals that span across days
    cross_day_duplicated_signals = cross_day_signals

    # Adjust next_timestamp to the end of the day for the original entries
    cross_day_signals = cross_day_signals.withColumn(
        "next_timestamp", F.expr("date_add(date, 1) - interval 1 second")
    )

    # Adjust timestamp to the start of the next day for the duplicated entries
    cross_day_duplicated_signals = cross_day_duplicated_signals.withColumn(
        "timestamp", F.expr("date_trunc('day', date_add(timestamp, 1))")
    )

    # Combine all the processed signals into one DataFrame
    all_signals = cross_day_duplicated_signals.union(cross_day_signals).union(same_day_signals)

    # Calculate the duration from the current timestamp to the next state
    all_signals = all_signals.withColumn(
        "duration_to_next_state",
        F.unix_timestamp("next_timestamp") - F.unix_timestamp("timestamp")
    )

    # Write the results to the specified output table, partitioning by the date column
    all_signals.write.format("delta").partitionBy("date").mode("overwrite").saveAsTable(table_calculated_timestamps)



def process_vehicle_state_data(spark, input_table_name: str, output_table_name: str):
    """
    Processes vehicle state data by renaming columns, computing new state columns based on conditions,
    and writing the result to a specified output table.
    
    Args:
        spark (SparkSession): The Spark session to use for data processing.
        input_table_name (str): The name of the input table containing the vehicle data.
        output_table_name (str): The name of the output table to write the processed data.
        
    Returns:
        None
    
    Raises:
        Py4JJavaError: If the input or output table names are incorrect or if the DataFrame operations fail.
    """
    # Load the DataFrame from the input table
    vehicle_data = spark.table(input_table_name)
    
    # Rename columns for better readability
    vehicle_data = vehicle_data.withColumnRenamed("XMM_SentinelMode_Status_Mobile", "gg_status") \
                               .withColumnRenamed("XMM_GearGuard_Video_Mode", "motioncam_setting")
    
    # Compute 'vehicle_state' column based on various conditions
    vehicle_data = vehicle_data.withColumn(
        "vehicle_state",
        F.when(
            (F.col("N_BMS_state") == "HV_Active_Idle") & 
            (F.col("context_user_proximity") == "remote") & 
            (F.col("N_contactor_state") == "AllContsClosed") & 
            (F.col("power_mode") == "ready"), "LVM"
        ).when(
            F.col("N_BMS_state") == "HV_Active_Drive", "Driving"
        ).when(
            (F.col("N_BMS_state") == "Shutdown") | 
            (F.col("N_BMS_state") == "Sleep"), "Sleep"
        ).when(
            (F.col("N_BMS_state") == "HV_Active_AC_Charging") | 
            (F.col("N_BMS_state") == "HV_Active_DC_Charging"), "Charging"
        ).when(
            F.col("N_BMS_state").isin(["Standby", "Initializing", "HV_Deactivation", 
                                        "HV_Activation", "HV_Active_DC_Charger_Activation", 
                                        "HV_Active_DC_Charger_Deactivation"]), "Transitory"
        ).otherwise("Catchall")
    )
    
    # Compute 'longest_gg_state' column based on 'gg_status' and 'motioncam_setting'
    vehicle_data = vehicle_data.withColumn(
        "longest_gg_state",
        F.when(
            (F.col("gg_status").isin(["Enabled", "Engaged"])) & (F.col("motioncam_setting") == "Everywhere"),
            "Enabled Everywhere"
        ).when(
            (F.col("gg_status").isin(["Enabled", "Engaged"])) & (F.col("motioncam_setting") == "Away_From_Home"),
            "Enabled Away From Home"
        ).otherwise("Disabled")
    )
    
    # Write the processed DataFrame to the output table
    vehicle_data.write.format("delta").mode("overwrite").saveAsTable(output_table_name)

def calculate_condensed_states(spark: SparkSession, source_table_name: str, destination_table_name: str) -> None:
    """
    Processes a dataset to remove consecutive duplicate vehicle states for each vehicle identified by VIN,
    and writes the condensed dataset to a new table.

    Args:
        spark (SparkSession): The Spark session used to execute data operations.
        source_table_name (str): The name of the source table that contains the vehicle states with columns
                                 'vin', 'timestamp', and 'vehicle_state'.
        destination_table_name (str): The name of the destination table where the condensed states will be stored.

    Returns:
        None

    Raises:
        AnalysisException: If the source table does not exist or does not have the required columns.
        Other pyspark.sql errors depending on the environment and the data operations performed.
    """
    # Read the source table into a DataFrame
    vehicle_states_df = spark.table(source_table_name)
    
    # Drop any rows with missing data
    vehicle_states_df = vehicle_states_df.dropna()
    
    # Define the window specification to partition by 'vin' and order by 'timestamp'
    window_spec = Window.partitionBy("vin").orderBy("timestamp")
    
    # Create a new column 'prev_vehicle_state' to hold the previous vehicle state within the window
    vehicle_states_df = vehicle_states_df.withColumn(
        "prev_vehicle_state", 
        F.lag("vehicle_state").over(window_spec)
    )
    
    # Filter out rows where the current vehicle state is the same as the previous state
    vehicle_states_df = vehicle_states_df.filter(
        F.col("vehicle_state") != F.col("prev_vehicle_state")
    )
    
    # Order the resulting DataFrame by 'vin' and 'timestamp'
    vehicle_states_df = vehicle_states_df.orderBy("vin", "timestamp")
    
    # Write the DataFrame to the destination table in 'delta' format partitioned by 'date'
    vehicle_states_df.write.format("delta").partitionBy("date").mode("overwrite").saveAsTable(destination_table_name)
    

def get_car_ownership_id_to_vin(vin_list):
    """
    Function to get car ownership IDs mapped to VINs for a given list of VINs.
    
    Args:
        vin_list (list of str): List of VINs to look up.
    
    Returns:
        dict: Dictionary mapping car ownership IDs to VINs.
    """
    # Import the required PySpark functions
    from pyspark.sql import functions as F
    
    # Read the mapping table
    mapping_table_df = spark.table("main.vehicles_all.anon_mappings")
    
    # Filter the DataFrame for the given VIN list
    filtered_df = mapping_table_df.filter(F.col("vin").isin(vin_list))
    
    # Select the VIN and car_ownership_id columns
    result_df = filtered_df.select("car_ownership_id", "vin")
    
    # Convert the result DataFrame to a Pandas DataFrame
    pandas_df = result_df.toPandas()
    
    # Convert the Pandas DataFrame to a dictionary
    car_ownership_id_to_vin = dict(zip(pandas_df['car_ownership_id'], pandas_df['vin']))
    
    return car_ownership_id_to_vin


def filter_vins_by_location(vin_dict, location):
    return [vin for vin, loc in vin_dict.items() if loc == location]

def add_calculated_columns(spark, table_name: str, output_table_name: str):
    # Read the input table
    df = spark.table(table_name)
    
    # Add the new column 'pure_HV_current_mA'
    df = df.withColumn(
        'pure_HV_current_mA',
        F.col('mini_dcdc_output_current_mA') - F.col('mini_dcdc_hsd_output_current_mA')
    )
    
    # Add the new column 'power_draw_W'
    df = df.withColumn(
        'power_draw_W',
        (F.col('mini_dcdc_output_voltage_V') * F.col('mini_dcdc_output_current_mA') / 1000).alias('power_draw_W')
    )
    
    # Write the updated DataFrame to a new table
    df.write.format("delta").partitionBy("date").mode("overwrite").saveAsTable(output_table_name)

def main():
    """
    Main function to configure Spark and execute data processing functions for vehicle telemetry data.

    This function:
    - Sets up the Spark session.
    - Defines table names and lists of signals to be processed.
    - Executes a sequence of data processing functions including signal extraction, processing, 
      renaming columns, and calculating various states and timestamps.
    """
    # Configure Spark session
    spark = create_configured_spark_session()
    
    # Define table names for various stages of processing
    table_low_latency = "main.telemetry.vehicle_low_latency_mapped"
    table_high_fidelity = "main.high_fidelity_rivian_vehicles_data.parsed_pcap_message_rivian_vehicles_v2"
    table_extracted_signals = "main.adhoc.ss_gg_extracted_signals_p_9"
    table_processed_signals = "main.adhoc.ss_gg_processed_signals_p_9"
    table_renamed_columns = "main.adhoc.ss_gg_grokkable_states_p_9"
    table_condensed_states = "main.adhoc.ss_gg_condensed_states_p_9"
    table_calculated_timestamps = "main.adhoc.ss_gg_calculated_timestamps_p_9"
    table_calculated_new_values = "main.adhoc.ss_gg_calculated_timestamps_and_new_values_p_9"
    table_calculated_condensed_timestamps = "main.adhoc.ss_gg_calculated_condensed_timestamps_p_9"
    
    # Define relevant columns to be used in processing
    relevant_columns = [
        "vin", "date", "timestamp", "property_value", "signal_name",
    ]
    
    # Define signals for various types
    motion_cam_signals = ["XMM_SentinelMode_Status_Mobile", "XMM_GearGuard_Video_Mode"]
    charge_signals = ["N_BMS_state", "N_contactor_state"]
    proximity_signals = ["context_user_proximity"]
    power_mode_signals = ["power_mode"]
    battery_soc_signals = ["E_SOE_pack_user_real", "rp_SOC_pack_user", "BMS_P_VehicleRange", "lv_charge_request", "context_lv_charge", "LV_battery_current"]
    charging_signals = ["charger_type_detected", "charging_mode_status"]
    # new_signals = ["mini_dcdc_output_current_mA"]
    new_signals = ["mini_dcdc_output_current_mA", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_voltage_V"]
    
    # Aggregate all signals into a single list for processing
    all_signals = (
        motion_cam_signals +
        power_mode_signals +
        charge_signals +
        battery_soc_signals +
        charging_signals +
        proximity_signals +
        new_signals
    )
    
    # Define the date range for data processing
    start_date, end_date = "2024-05-01", "2024-05-16"

    vin_dict = {
    "7PDSGJBA9RMT0322X": "TAPG",
    "7PDSGJBA1RMT0323X": "Waterworks",
    "7PDSGBBAXPMD0202X": "Palo Alto",
    "7FCTGBAA6PMD0191X": "Palo Alto",
    "7FCTGBAA3PMD0190X": "Fairbanks, Alaska",
    "7FCTGBAA4PMD0189X": "Palo Alto",
    "7FCTGBAA1PMD0188X": "Waterworks",
    "7FCTGBAA7PMD0174X": "Myford 2",
    "7FCTGGAA9PMT0274X": "Palo Alto",
    "7FCTGEAA1PMT0277X": "Palo Alto",
    "7PDSGBBA0PMD0193X": "Fairbanks, Alaska",
    "7PDSGBBA4PMD0200X": "Waterworks",
    "7PDSGBBA8PMD0145X": "Palo Alto",
    "7PDSGEBA2PMT0253X": "TAPG",
    "7PDSGGBA0PMT0254X": "TAPG",
    "7PDSGGBA6PMT0256X": "Fairbanks, Alaska",
    "7PDSGGBA9PMT0257X": "Myford 2",
    "7PDSGJBA2RMT0348X": "Myford 2",
    "7PDSGBBA7PMD0183X": "TAPG",
    "7PDSGBBA7PMD0201X": "Palo Alto",
    "7FCTGBAA3PMD0122X": "Palo Alto",
    "7FCTGGAA8PMT0270X": "TAPG",
    "7FCTGFAA8PMT0266X": "Palo Alto",
    "7FCTGFAA9PMT0280X": "TAPG",
    "7FCTGBAAXPMD0175X": "TAPG",
    "7FCTGBAA4PMD0126X": "Palo Alto",
    "7FCTGEAA5PMT0305X": "TAPG",
    "7PDSGFBA7PMT0292X": "Myford 2",
    "7PDSGBBA2PMD0203X": "TAPG",
    "7PDSGGBA7PMT0286X": "Palo Alto",
    "7PDSGGBA8PMT0295X": "TAPG",
    "7PDSGGBA3PMT0297X": "Myford 2",
    "7FCTGGAA3PMT0272X": "TAPG",
    "7FCTGGAA7PMT0261X": "Fairbanks, Alaska",
    "7PDSGJBA4RMT0324X": "Waterworks",
    "7PDSGFBA4PMT0291X": "TAPG",
    "7PDSGEBA7PMT0293X": "Fairbanks, Alaska",
    "7FCTGJAA8RMT0330X": "Palo Alto",
    "7FCTGJAA0RMT0331X": "Palo Alto",
    "7PDSGGBA3PMT0255X": "Palo Alto",
    "7FCTGEAAXPMT0303X": "Plymouth",
    "7PDSGABA5PMD0226X" : "Palo Alto"
    }

    vin_list = filter_vins_by_location(vin_dict, "Palo Alto")

    # Define filter conditions for extracting data
    original_conditions = (
        (F.col("vin").isin(vin_list)) &
        (F.col("date") >= F.lit(start_date)) &
        (F.col("date") <= F.lit(end_date))
    )
    
    car_ownership_id_to_vin = get_car_ownership_id_to_vin(vin_list)
    
    # Step 1: Pull signals from various sources
    # pull_signals(
    #     spark,
    #     table_low_latency,
    #     table_high_fidelity,
    #     table_extracted_signals,
    #     all_signals,
    #     original_conditions,
    #     relevant_columns,
    #     new_signals,
    #     car_ownership_id_to_vin,
    # )
    logging.info("Step 1: Signals extracted successfully.")
    
    # Step 2: Process signals
    # process_signals(
    #     spark, table_extracted_signals, table_processed_signals, all_signals
    # )
    logging.info("Step 2: Signals processed successfully.")
    
    # Step 3: Rename the columns
    # process_vehicle_state_data(spark, table_processed_signals, table_renamed_columns)
    logging.info("Step 3: Columns renamed successfully.")
    
    # Step 4: Calculate condensed states
    # calculate_condensed_states(spark, table_renamed_columns, table_condensed_states)
    logging.info("Step 4: Condensed states calculated successfully.")
    
    # Step 5: Calculate full timestamps
    # calculate_timestamps(spark, table_renamed_columns, table_calculated_timestamps)
    logging.info("Step 5: Full timestamps calculated successfully.")

    # Step 6: Calculate the new values
    add_calculated_columns(spark, table_calculated_timestamps, table_calculated_new_values)
    logging.info("Step 6: Wattage and Battery Draw Calculated")
    
    # Step 6: Calculate condensed timestamps
    # calculate_timestamps(spark, table_calculated_new_values, table_calculated_condensed_timestamps)
    # logging.info("Step 6: Condensed timestamps calculated successfully.")


if __name__ == "__main__":
    main()
