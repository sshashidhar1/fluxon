# Databricks notebook source
import standard_defs as sdx
from pyspark.sql import DataFrame
from pyspark.sql.window import Window
import pyspark.sql.functions as F
from pyspark.sql import functions as F
from itertools import chain

# COMMAND ----------

# MAGIC %md
# MAGIC Here is a list of peregrine VINs defined

# COMMAND ----------

df = spark.table("main.adhoc.wifi_lte_sleep_distribution")

# COMMAND ----------

df = df.filter(df.vin == '7PDSGABL2NN000117')

# COMMAND ----------

display(df)

# COMMAND ----------

vins = [
    "7PDSGBBA7SN045490",  # Matt Wolfe
    "7PDSGBBA7SN045456",  # Software (Mark Milne & Thien Vo)
    "7PDSGBBA2SN045428",  # Greg Dachner
    "7PDSGBBA5SN045472",  # Katie McKinstry
    "7PDSGBBA6SN045397",  # Brad Busboom
    "7PDSGBBA5SN045486",  # Christina Yu
    "7PDSGBBA9SN045491",  # Andrew Schiller
    "7PDSGBBA2SN045395",  # Scott Rioux
    "7PDSGBBA0SN045444",  # Kristoffer Gronowski
    "7PDSGBBA7SN045456",  # Mark Annis
    "7PDSGBBAXSN045385",  # Chris Jenny
    "7PDSGBBA1SN045436",  # Mike Spillane
    "7PDSGBBA2SN045364",  # Genna Peltier
    "7PDSGBBA9SN045393",  # Nick Slovan
    "7PDSGBBA0SN045475",  # Akash Mathur
    "7PDSGBBA6SN045383",  # Nick Sands
    "7PDSGBBA1SN045369",  # Ishan Gosain
    "7PDSGBBA6SN045450",  # Carlos Montes Relanzon
    "7PDSGBBA4SN045415",  # Eric Spenceley
    "7PDSGBBA4SN045365",  # Daniel Yoon
    "7PDSGBBA4SN045379",  # Zulf Ali
    "7PDSGBBA5SN045455",  # Sam Jones
    "7PDSGBBA1SN045405",  # Ricardo DeMatos
    "7PDSGBBA6SN045352",  # Diana Cuellar Alcala
    "7PDSGBBA5SN045407",  # Garren Salibian
    "7PDSGBBAXSN045399",  # Christie Taylor
    "7PDSGBBA5SN045438",  # Vishwas Venkatachalapathy
    "7PDSGBBA7SN045358",  # Jeff Hammoud
    "7PDSGBBA7SN045392",  # Calvin Kohring
    "7PDSGBBA0SN045430",  # Cesar Rodriguez
    "7PDSGBBA3SN045440",  # Jesus Gomez
    "7PDSGBBAXSN045497",  # Jonathan Verghese
    "7PDSGBBA6SN045495",  # Daniel Santos
    "7PDSGBBA2SN045350",  # Merry Mac Miller
    "7PDSGBBA9SN045443",  # Mario Carrera
    "7PDSGBBA2SN045445",  # Dan Boyer
    "7PDSGBBA0SN045461",  # Nithin Chemmanoor
    "7PDSGBBA6SN045402",  # Megan Onstott
    "7PDSGBBA3SN045485",  # Mithun Sunny
    "7PDSGBBA3SN045387",  # Dinesh Sathia Raj
    "7PDSGBBA0SN045489",  # Chris Mandich
    "7PDSGBBAXSN045421",  # Mohammad Abdelrahman
    "7PDSGBBA3SN045454",  # Denise Cherry
    "7PDSGBBA4SN045429",  # Zach Stuckey-Hughes
    "7PDSGBBA4SN045463",  # Han Xu
    "7PDSGBBA0SN045394",  # Cole Maas
    "7PDSGBBAXSN045452",  # Lou Scanlon
    "7PDSGBBA1SN045372",  # Clinton Hollister
    "7PDSGBBA5SN045438",  # Tom Solomon
    "7PDSGBBA0SN045492",  # Adam Koehler
    "7PDSGBBA1SN045422",  # Ghassan Khreis - works
    "7PDSGBBA8SN045479",  # Nick Chenoweth
    "7PDSGBBA4SN045446",  # Josh Haviland
    "7PDSGBBA8SN045501",  # Takeshi Ouchi
    "7PDSGBBA1SN045386",  # Nina Bruno
    "7PDSGBBA3SN045437",  # Max Monarrez
    "7PDSGBBA1SN045453",  # Victor Rojas
    "7PDSGBBA9SN045460",  # Leonid Ganapolskiy
    "7PDSGBBA0SN045394",  # Divinitha Sreenivas
    "7PDSGBBA0SN045461",  # Josh Crater
    "7PDSGBBA9SN045393",  # Tara Hashemi
    "7PDSGBBA4SN045382",  # Beau Christensen
    "7PDSGBBA2SN045459",  # Nenad Uzunovic
    "7PDSGBBA4SN045429",  # Oscar Santana
    "7PDSGBBA9SN045409",  # Andre Ramdhanny
    "7PDSGBBA7SN045408",  # Jess Denison
    "7PDSGBBA8SN045384",  # Brian Hopkins
]


# COMMAND ----------

# MAGIC %md
# MAGIC Here is a list of defined signals

# COMMAND ----------

telemetry_signals = [
    "XMM_SentinelMode_Status_Mobile",
    "XMM_GearGuard_Video_Mode",
    "N_BMS_state",
    "N_contactor_state",
    "E_SOE_pack_user_real",

    # these are now the minidcdc signals.
    # first, current
    "mini_dcdc_output_current_mA",
    "mini_dcdc_hsd_output_current_mA",

    # then, the output voltages
    "mini_dcdc_output_voltage_V",
    "mini_dcdc_low_voltage_12v_battery_voltage_V",

    # then, the states
    "mini_dcdc_state", 
    "mini_dcdc_hsd_state"
]

# COMMAND ----------

# MAGIC %md
# MAGIC Here are the defined table, and the selected columns, post filtering

# COMMAND ----------

telemetry_table = "main.telemetry.vehicle_low_latency_mapped"
output_filtered_telemetry_table = "main.adhoc.ssh_peregrine_gg_eeap_telemetry_selective"
pivoted_output_filtered_telemetry_table = "main.adhoc.ssh_peregrine_gg_eeap_telemetry_selective_pivoted"
telemetry_selected_columns = ["vin", "date", "timestamp", "signal_name", "property_value"]

# COMMAND ----------

# MAGIC %md
# MAGIC Let's just filter for a set of VINs

# COMMAND ----------

def filter_df_for_vins(spark, vinlist, table, signals, selected_columns):
    df = spark.table(table)
    df_filtered_by_date = df.filter(F.to_date(F.col("timestamp")) >= "2024-05-15")
    df_filtered_by_vins = df_filtered_by_date.filter(F.col("vin").isin(vinlist))
    df_filtered_by_signals = df_filtered_by_vins.filter(
        F.col("signal_name").isin(signals)
    )
    df_selected = df_filtered_by_signals.select(selected_columns)
    return df_selected

# COMMAND ----------

# MAGIC %md
# MAGIC Lets run this code in a loop

# COMMAND ----------

spark = sdx.create_configured_spark_session()
# filter the df
df_telemetry = filter_df_for_vins(
    spark, vins, telemetry_table, telemetry_signals, telemetry_selected_columns
)
df_telemetry.write.mode("overwrite").partitionBy("vin").saveAsTable(output_filtered_telemetry_table)

# COMMAND ----------

# MAGIC %md
# MAGIC Now, to actually analyze this properly, lets pivot the data

# COMMAND ----------

def pivot_and_fill_telemetry_data(table_name: str) -> DataFrame:
    # Load the table
    df = spark.table(table_name)
    
    # Pivot the signal_name column
    pivot_df = df.groupBy("vin", "date", "timestamp") \
                 .pivot("signal_name") \
                 .agg(F.first("property_value"))
    
    # Define window specification for filling null values within each VIN partition
    window_spec = Window.partitionBy("vin").orderBy("timestamp").rowsBetween(Window.unboundedPreceding, Window.currentRow)
    
    
    # Cast necessary columns to double
    double_cols = [
        "mini_dcdc_output_voltage_V",
        "mini_dcdc_output_current_mA",
        "mini_dcdc_hsd_output_current_mA",
        "mini_dcdc_low_voltage_12v_battery_voltage_V",
        "E_SOE_pack_user_real"
    ]
    
    for col in double_cols:
        pivot_df = pivot_df.withColumn(col, F.col(col).cast("double"))
    
    # Calculate mini_dcdc_power_draw_W
    pivot_df = pivot_df.withColumn("mini_dcdc_power_draw_W",
                                   F.col("mini_dcdc_output_voltage_V") * (F.col("mini_dcdc_output_current_mA") / 1000))
    
    # Ensure the DataFrame is ordered by timestamp within each vin partition
    window_spec_final_order = Window.partitionBy("vin").orderBy("timestamp")
    pivot_df = pivot_df.withColumn("row_num", F.row_number().over(window_spec_final_order)) \
                       .orderBy("vin", "row_num").drop("row_num")
    
    return pivot_df

# COMMAND ----------

result_df = pivot_and_fill_telemetry_data(output_filtered_telemetry_table)
result_df.write.mode("overwrite").partitionBy("vin").saveAsTable(pivoted_output_filtered_telemetry_table)

# COMMAND ----------


