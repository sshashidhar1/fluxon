from pyspark.sql import DataFrame
from pyspark.sql.window import Window
import pyspark.sql.functions as F

def pivot_and_fill_telemetry_data(table_name: str) -> DataFrame:
    # Load the table
    df = spark.table(table_name)
    
    # Pivot the signal_name column
    pivot_df = df.groupBy("vin", "date", "id", "timestamp") \
                 .pivot("signal_name") \
                 .agg(F.first("property_value"))

    # Define window specification for filling null values within each VIN partition
    window_spec = Window.partitionBy("vin").orderBy("timestamp").rowsBetween(Window.unboundedPreceding, Window.currentRow)
    
    # Forward fill
    fill_cols = pivot_df.columns[4:]  # Columns that were created from pivot, skip first 4 original columns
    for col in fill_cols:
        pivot_df = pivot_df.withColumn(col, F.last(col, ignorenulls=True).over(window_spec))
    
    # Backward fill (optional, uncomment if needed)
    # for col in fill_cols:
    #     pivot_df = pivot_df.withColumn(col, F.first(col, ignorenulls=True).over(window_spec))
    
    # Cast the necessary columns to double
    pivot_df = pivot_df.withColumn("rp_SOC_pack_user", F.col("rp_SOC_pack_user").cast("double"))
    pivot_df = pivot_df.withColumn("mini_dcdc_output_voltage_V", F.col("mini_dcdc_output_voltage_V").cast("double"))
    pivot_df = pivot_df.withColumn("mini_dcdc_output_current_mA", F.col("mini_dcdc_output_current_mA").cast("double"))
    pivot_df = pivot_df.withColumn("mini_dcdc_low_voltage_12v_battery_voltage_V", F.col("mini_dcdc_low_voltage_12v_battery_voltage_V").cast("double"))
    pivot_df = pivot_df.withColumn("mini_dcdc_hsd_output_current_mA", F.col("mini_dcdc_hsd_output_current_mA").cast("double"))
    pivot_df = pivot_df.withColumn("E_SOE_pack_user_real", F.col("E_SOE_pack_user_real").cast("double"))

    # Calculate mini_dcdc_power_draw_W
    pivot_df = pivot_df.withColumn("mini_dcdc_power_draw_W",
                                   (F.col("mini_dcdc_output_voltage_V") * (F.col("mini_dcdc_output_current_mA") / 1000)))

    # Ensure the DataFrame is ordered by timestamp within each vin partition
    window_spec_final_order = Window.partitionBy("vin").orderBy("timestamp")
    pivot_df = pivot_df.withColumn("row_num", F.row_number().over(window_spec_final_order)).orderBy("vin", "row_num").drop("row_num")

    return pivot_df

# Usage
result_df = pivot_and_fill_telemetry_data("main.adhoc.ss_filtered_eeap_peregrine_telemetry")
result_df.write.mode("overwrite").partitionBy("vin").saveAsTable("main.adhoc.ss_pivoted_filled_eeap_peregrine_telemetry")
