from pyspark.sql import SparkSession
from pyspark.sql import functions as F


def create_configured_spark_session() -> SparkSession:
    """
    Initializes and configures a Spark session with specific settings optimized for Delta tables.

    Returns:
        SparkSession: A Spark session configured with specific optimizations for Delta tables.
    """
    # Create or retrieve the existing Spark session
    spark = SparkSession.builder.getOrCreate()

    # Set configuration properties specific to Delta tables optimization
    spark.conf.set("spark.databricks.delta.optimizeWrite.enabled", "true")
    spark.conf.set("spark.databricks.delta.autoOptimize.optimizeWrite", "true")

    # Return the configured Spark session
    return spark


def get_car_ownership_id_to_vin(
    spark, vin_list, mapping_table="main.vehicles_all.anon_mappings"
):
    """
    Function to get car ownership IDs mapped to VINs for a given list of VINs.

    Args:
        vin_list (list of str): List of VINs to look up.

    Returns:
        dict: Dictionary mapping car ownership IDs to VINs.
    """
    # Read the mapping table
    mapping_table_df = spark.table(mapping_table)

    # Filter the DataFrame for the given VIN list
    filtered_df = mapping_table_df.filter(F.col("vin").isin(vin_list))

    # Select the VIN and car_ownership_id columns
    result_df = filtered_df.select("car_ownership_id", "vin")

    # Convert the result DataFrame to a Pandas DataFrame
    pandas_df = result_df.toPandas()

    # Convert the Pandas DataFrame to a dictionary
    car_ownership_id_to_vin = dict(zip(pandas_df["car_ownership_id"], pandas_df["vin"]))

    return car_ownership_id_to_vin
