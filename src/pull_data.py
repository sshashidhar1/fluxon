# import standard definitions
import standard_defs as sdx
from pyspark.sql import functions as F
from itertools import chain

vins = [
    "7PDSGBBA7SN045490",  # Matt Wolfe
    "7PDSGBBA7SN045456",  # Software (Mark Milne & Thien Vo)
    "7PDSGBBA2SN045428",  # Greg Dachner
    "7PDSGBBA5SN045472",  # Katie McKinstry
    "7PDSGBBA6SN045397",  # Brad Busboom
    "7PDSGBBA5SN045486",  # Christina Yu
    "7PDSGBBA9SN045491",  # Andrew Schiller
    "7PDSGBBA2SN045395",  # Scott Rioux
    "7PDSGBBA0SN045444",  # Kristoffer Gronowski
    "7PDSGBBA7SN045456",  # Mark Annis
    "7PDSGBBAXSN045385",  # Chris Jenny
    "7PDSGBBA1SN045436",  # Mike Spillane
    "7PDSGBBA2SN045364",  # Genna Peltier
    "7PDSGBBA9SN045393",  # Nick Slovan
    "7PDSGBBA0SN045475",  # Akash Mathur
    "7PDSGBBA6SN045383",  # Nick Sands
    "7PDSGBBA1SN045369",  # Ishan Gosain
    "7PDSGBBA6SN045450",  # Carlos Montes Relanzon
    "7PDSGBBA4SN045415",  # Eric Spenceley
    "7PDSGBBA4SN045365",  # Daniel Yoon
    "7PDSGBBA4SN045379",  # Zulf Ali
    "7PDSGBBA5SN045455",  # Sam Jones
    "7PDSGBBA1SN045405",  # Ricardo DeMatos
    "7PDSGBBA6SN045352",  # Diana Cuellar Alcala
    "7PDSGBBA5SN045407",  # Garren Salibian
    "7PDSGBBAXSN045399",  # Christie Taylor
    "7PDSGBBA5SN045438",  # Vishwas Venkatachalapathy
    "7PDSGBBA7SN045358",  # Jeff Hammoud
    "7PDSGBBA7SN045392",  # Calvin Kohring
    "7PDSGBBA0SN045430",  # Cesar Rodriguez
    "7PDSGBBA3SN045440",  # Jesus Gomez
    "7PDSGBBAXSN045497",  # Jonathan Verghese
    "7PDSGBBA6SN045495",  # Daniel Santos
    "7PDSGBBA2SN045350",  # Merry Mac Miller
    "7PDSGBBA9SN045443",  # Mario Carrera
    "7PDSGBBA2SN045445",  # Dan Boyer
    "7PDSGBBA0SN045461",  # Nithin Chemmanoor
    "7PDSGBBA6SN045402",  # Megan Onstott
    "7PDSGBBA3SN045485",  # Mithun Sunny
    "7PDSGBBA3SN045387",  # Dinesh Sathia Raj
    "7PDSGBBA0SN045489",  # Chris Mandich
    "7PDSGBBAXSN045421",  # Mohammad Abdelrahman
    "7PDSGBBA3SN045454",  # Denise Cherry
    "7PDSGBBA4SN045429",  # Zach Stuckey-Hughes
    "7PDSGBBA4SN045463",  # Han Xu
    "7PDSGBBA0SN045394",  # Cole Maas
    "7PDSGBBAXSN045452",  # Lou Scanlon
    "7PDSGBBA1SN045372",  # Clinton Hollister
    "7PDSGBBA5SN045438",  # Tom Solomon
    "7PDSGBBA0SN045492",  # Adam Koehler
    "7PDSGBBA1SN045422",  # Ghassan Khreis
    "7PDSGBBA8SN045479",  # Nick Chenoweth
    "7PDSGBBA4SN045446",  # Josh Haviland
    "7PDSGBBA8SN045501",  # Takeshi Ouchi
    "7PDSGBBA1SN045386",  # Nina Bruno
    "7PDSGBBA3SN045437",  # Max Monarrez
    "7PDSGBBA1SN045453",  # Victor Rojas
    "7PDSGBBA9SN045460",  # Leonid Ganapolskiy
    "7PDSGBBA0SN045394",  # Divinitha Sreenivas
    "7PDSGBBA0SN045461",  # Josh Crater
    "7PDSGBBA9SN045393",  # Tara Hashemi
    "7PDSGBBA4SN045382",  # Beau Christensen
    "7PDSGBBA2SN045459",  # Nenad Uzunovic
    "7PDSGBBA4SN045429",  # Oscar Santana
    "7PDSGBBA9SN045409",  # Andre Ramdhanny
    "7PDSGBBA7SN045408",  # Jess Denison
    "7PDSGBBA8SN045384",  # Brian Hopkins
]

telemetry_signals = [
    "XMM_SentinelMode_Status_Mobile",
    "XMM_GearGuard_Video_Mode",
    "N_BMS_state",
    "N_contactor_state",
    "power_mode",
    "E_SOE_pack_user_real",
    "rp_SOC_pack_user",
    "charging_mode_status",
]

telemetry_table = "main.telemetry.vehicle_low_latency_mapped"
filtered_telemetry_table = "main.adhoc.ss_filtered_eeap_peregrine_telemetry"
telemetry_selected_columns = ["vin", "date", "id", "timestamp", "signal_name", "property_value"]

high_fidelity_table = "main.high_fidelity_rivian_vehicles_data.parsed_pcap_message_rivian_vehicles_v2"
high_fid_signals = ["mini_dcdc_output_current_mA", "mini_dcdc_hsd_output_current_mA", "mini_dcdc_output_voltage_V", "mini_dcdc_low_voltage_12v_battery_voltage_V",  
"mini_dcdc_state", "mini_dcdc_hsd_state",]

telemetry_signals = telemetry_signals + high_fid_signals

def filter_df_for_vins(spark, vinlist, table, signals, selected_columns):
    df = spark.table(table)
    df_filtered_by_date = df.filter(F.to_date(F.col("timestamp")) >= "2024-05-10")
    df_filtered_by_vins = df_filtered_by_date.filter(F.col("vin").isin(vinlist))
    df_filtered_by_signals = df_filtered_by_vins.filter(
        F.col("signal_name").isin(signals)
    )
    df_selected = df_filtered_by_signals.select(selected_columns)
    return df_selected

def filter_high_fidelity_data(spark, car_ownership_ids_to_vins, table, new_signals):
    df = spark.table(table)
    df_filtered_by_date = df.filter(F.to_date(F.col("timestamp")) == "2024-05-16")
    # Filter by car_ownership_ids (id) and new_signals (signal.name)
    df_filtered_by_ids = df_filtered_by_date.filter(F.col("id").isin(list(car_ownership_ids_to_vins.keys())))
    df_filtered_by_signals = df_filtered_by_ids.filter(F.col("signal.name").isin(new_signals))
    
    # Add vin column using car_ownership_ids_to_vins mapping
    mapping_expr = F.create_map([F.lit(x) for x in chain(*car_ownership_ids_to_vins.items())])
    df_with_vin = df_filtered_by_signals.withColumn("vin", mapping_expr.getItem(F.col("id")))

    df_with_vin = df_with_vin.withColumn("date", F.to_date(F.col("timestamp")))
    
    # Select and rename necessary columns to match the telemetry table schema
    df_selected = df_with_vin.select(
        F.col("vin"),
        F.col("date"),
        F.col("id"),
        F.col("timestamp"),
        F.col("signal.name").alias("signal_name"),
        F.col("signal.value").alias("property_value"),
    )
    
    return df_selected

def main():
    # this should be the only thing you need to look at, to understand
    # program flow
    spark = sdx.create_configured_spark_session()
    # get ownership ids
    car_ownership_ids_to_vins = sdx.get_car_ownership_id_to_vin(spark, vins)
    # filter the df
    df_telemetry = filter_df_for_vins(
        spark, vins, telemetry_table, telemetry_signals, telemetry_selected_columns
    )
    # df_high_fidelity = filter_high_fidelity_data(
    #     spark, car_ownership_ids_to_vins, high_fidelity_table, high_fid_signals
    # )

    # df_concatenated = df_telemetry.unionByName(df_high_fidelity)
    df_concatenated = df_telemetry
    df_sorted = df_concatenated.orderBy("timestamp")
    df_sorted.write.mode("overwrite").partitionBy("vin").saveAsTable(filtered_telemetry_table)



if __name__ == "__main__":
    main()
